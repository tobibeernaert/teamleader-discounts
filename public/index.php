<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \CodingTest\Validator as ValidatorFactory;

define('WEBROOT', realpath(__DIR__.'/../'));

require '../vendor/autoload.php';

$config = (require '../app/config/config.php');
$app = new \Slim\App($config);
require '../app/config/container.php';

$app->post('/discounts', function (Request $request, Response $response) use ($app) {
    $order = $request->getAttribute('order');

    $discountCalculator = new \CodingTest\DiscountCalculator;
    foreach ($this->ruleService->findActiveRules() as $rule) {
        $discountCalculator->addRule($rule);
    }
    $discountCalculator->applyRules($order);

    return $response->withJson([
        'applied_rules' => $discountCalculator->getAppliedRules(),
        'discount' => $discountCalculator->getDiscount(),
    ]);
})->add(
    new \CodingTest\Middleware\LoadOrder(
        $container->orderService
    )
)->add(
    new \CodingTest\Middleware\ValidationErrorHandling
)->add(
    new \DavidePastore\Slim\Validation\Validation(
        ValidatorFactory::buildOrderValidators()
    )
);
$app->run();
