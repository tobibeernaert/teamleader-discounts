# Discounts

Microservice to calculate discounts.  
See [issue](./ISSUE.md) for more info.

## Installation

```
git clone git@bitbucket.org:tobibeernaert/teamleader-discounts.git discounts
cd discounts
composer install
```

## Usage

Run PHP built-in web server.
```
php -S localhost:8080 -t public
```

Execute a post to localhost:8080/discounts with an order in the body.
```
curl -X POST -H "Content-Type: application/json" -H "Cache-Control: no-cache" -d '{
    "id": "1",
    "customer-id": "1",
    "items": [
        {
            "product-id": "A101",
            "quantity": "6",
            "unit-price": "2.99",
            "total": "29.90"
        },
        {
            "product-id": "B102",
            "quantity": "2",
            "unit-price": "3.99",
            "total": "39.90"
        }
    ],
    "total": "49.90"
}'
"http://localhost:8080/discounts"
```

Valid Response.
```
{
    "applied_rules": {
        "rule title": 10.00
    },
    "discount": 10.00
}
```

In case of validation errors, a 500 will be thrown with the validation errors in
the body.

```
{
    "Validation error": {
        "key": [
            "error message:"
        ]
    }
}
```

## Rules & Data

Currently there are 3 rules set up:  

- A customer who has already bought for over € 1000, gets a discount of 10% on the whole order.
- For every products of category "Switches" (id 2), when you buy five, you get a sixth for free.
- If you buy two or more products of category "Tools" (id 1), you get a 20% discount on the cheapest product.

Customer, Product and rule data can be found in the [data folder](./src/data).  
Examples are located in the [example folder](./examples)
