<?php
$container = $app->getContainer();

$container['productService'] = function ($container) {
    $rawJson = file_get_contents(WEBROOT.'/data/products.json');
    return new \CodingTest\Mapper\Product(json_decode($rawJson, true));
};

$container['customerService'] = function ($container) {
    $rawJson = file_get_contents(WEBROOT.'/data/customers.json');
    return new \CodingTest\Mapper\Customer(json_decode($rawJson, true));
};

$container['orderService'] = function ($container) {
    return new \CodingTest\Mapper\Order(
        $container->productService,
        $container->customerService
    );
};

$container['ruleService'] = function ($container) {
    $rawJson = file_get_contents(WEBROOT.'/data/rules.json');
    return new \CodingTest\Mapper\Rule(json_decode($rawJson, true));
};
