<?php
namespace CodingTest\Entity;

/**
 * Order
 */
class Order
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var \CodingTest\Entity\Customer
     */
    protected $customer;

    /**
     * @var float
     */
    protected $total;

    /**
     * @var arrray
     */
    protected $items;

    /**
     * @param int $id
     * @param \CodingTest\Entity\Customer $customer
     * @param float $total
     *
     * @return void
     */
    public function __construct($id, \CodingTest\Entity\Customer $customer, $total)
    {
        $this->id = $id;
        $this->customer = $customer;
        $this->total = $total;
        $this->items = [];
    }

    /**
     * Id getter.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Customer getter.
     *
     * @return \CodingTest\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }
    
    /**
     * Add order item to order.
     *
     * @param \CodingTest\Entity\Product $product
     * @param int $quantity
     * @param float $unitPrice
     * @param float $total
     *
     * @return void
     */
    public function addItem(
        \CodingTest\Entity\Product $product,
        $quantity,
        $unitPrice,
        $total
    ) {
        $this->items[$product->getId()] = (object)[
            "product" => $product,
            "quantity" => $quantity,
            "unitPrice" => $unitPrice,
            "total" =>$total,
        ];
    }

    /**
     * Items getter.
     *
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }
    
    /**
     * Total getter.
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }
    
    /**
     * Check if order has any items attached.
     *
     * @return boolean
     */
    public function hasItems()
    {
        return count($this->items) > 0;
    }
}
