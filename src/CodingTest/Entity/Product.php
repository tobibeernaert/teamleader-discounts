<?php
namespace CodingTest\Entity;

/**
 * Product
 */
class Product
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var int
     */
    protected $category;

    /**
     * @var float
     */
    protected $price;

    /**
     * @param string $id
     * @param float $price
     *
     * @return void
     */
    public function __construct($id, $price)
    {
        $this->setId($id);
        $this->setPrice($price);
    }

    /**
     * Id getter.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Id setter.
     *
     * @param int $id
     * @return void
     */
    public function setId($id)
    {
        $this->id = (int)$id;
    }

    /**
     * Description getter.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Description setter.
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Category getter.
     *
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    /**
     * Category setter.
     *
     * @param int $category
     * @return void
     */
    public function setCategory($category)
    {
        $this->category = (int)$category;
    }

    /**
     * Check if product is in given category.
     *
     * @param int $categoryId
     * @return boolean
     */
    public function inCategory($categoryId)
    {
        return ((int)$categoryId === (int)$this->getCategory());
    }

    /**
     * Price getter.
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }
    
    /**
     * Price setter.
     *
     * @param float $price
     * @return void
     */
    public function setPrice($price)
    {
        $this->price = (float)$price;
    }
}
