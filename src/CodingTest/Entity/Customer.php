<?php
namespace CodingTest\Entity;

/**
 * Customer
 */
class Customer
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $since;

    /**
     * @var float
     */
    protected $revenue;

    /**
     * @string string $name
     *
     * @return void
     */
    public function __construct($name)
    {
        $this->setName($name);
    }

    /**
     * Id getter.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Id setter.
     *
     * @param int $id
     * @return void
     */
    public function setId($id)
    {
        $this->id = (int)$id;
    }
    
    /**
     * Name getter.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Name setter.
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * Since getter.
     *
     * @return string
     */
    public function getSince()
    {
        return $this->since;
    }
    
    /**
     * Since setter.
     *
     * @param string $since
     * @return void
     */
    public function setSince($since)
    {
        $this->since = $since;
    }
    
    /**
     * Revenue getter.
     *
     * @return float
     */
    public function getRevenue()
    {
        return $this->revenue;
    }
    
    /**
     * Revenue setter.
     *
     * @param float $revenue
     * @return void
     */
    public function setRevenue($revenue)
    {
        $this->revenue = (float)$revenue;
    }
}
