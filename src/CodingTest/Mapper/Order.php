<?php
namespace CodingTest\Mapper;

/**
 * Order Mapper.
 */
class Order
{
    /**
     * @var \CodingTest\Mapper\Customer
     */
    protected $customerService;

    /**
     * @var \CodingTest\Mapper\Product
     */
    protected $productService;

    /**
     * @param \CodingTest\Mapper\Product $productService
     * @param \CodingTest\Mapper\Customer $customerService
     *
     * @return void
     */
    public function __construct(
        \CodingTest\Mapper\Product $productService,
        \CodingTest\Mapper\Customer $customerService
    ) {
        $this->customerService = $customerService;
        $this->productService = $productService;
    }

    /**
     * Full load of a single order entity.
     *
     * @param Array $orderData
     *
     * @return \CodingTest\Entity\Order
     */
    public function loadOrder($orderData)
    {
        $order = new \CodingTest\Entity\Order(
            $orderData['id'],
            $this->customerService->findCustomerById($orderData['customer-id']),
            $orderData['total']
        );

        foreach ($orderData['items'] as $item) {
            $order->addItem(
                $this->productService->findProductById($item['product-id']),
                $item['quantity'],
                $item['unit-price'],
                $item['total']
            );
        }

        return $order;
    }
}
