<?php
namespace CodingTest\Mapper;

/**
 * Product Mapper.
 */
class Product
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Check if product X is a child of Y category.
     *
     * @param mixed $productId
     * @param int $categoryId
     *
     * @return boolean
     */
    public function inCategory($productId, $categoryId)
    {
        $product = $this->findProductById($productId);

        return (null !== $product && $categoryId === (int)$product->getCategory());
    }

    /**
     * Find product from it's ID.
     *
     * @param mixed $productId
     *
     * @return mixed
     */
    public function findProductById($productId)
    {
        foreach ($this->data as $row) {
            if ($row['id'] === $productId) {
                $product = new \CodingTest\Entity\Product(
                    $row['id'],
                    $row['price']
                );
                $product->setDescription($row['description']);
                $product->setCategory($row['category']);
                
                return $product;
            }
        }

        return null;
    }

    /**
     * Get product collection based on category ID.
     *
     * @param int $categoryId
     *
     * @return array
     */
    public function filterProductsByCategoryId($categoryId)
    {
        return array_filter($this->data, function($row) use ($categoryId) {
            return ($categoryId === (int)$row['category']);
        });
    }
}
