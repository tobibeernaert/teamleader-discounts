<?php
namespace CodingTest\Mapper;

/**
 * Customer Mapper
 */
class Customer
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Find customer by it's identifier.
     *
     * @param int $customerId
     *
     * @return mixed
     */
    public function findCustomerById($customerId)
    {
        foreach ($this->data as $row) {
            if ((int)$customerId === (int)$row['id']) {
                $customer = new \CodingTest\Entity\Customer(
                    $row['name']
                );
                $customer->setId($row['id']);
                $customer->setSince($row['since']);
                $customer->setRevenue($row['revenue']);

                return $customer;
            }
        }

        return null;
    }
}
