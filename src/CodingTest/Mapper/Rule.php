<?php
namespace CodingTest\Mapper;

/**
 * Rule Mapper.
 */
class Rule
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Find all active rules from data source.
     *
     * @return array
     */
    public function findActiveRules()
    {
        $activeRules = [];
        foreach ($this->data as $rule) {
            if ($rule['active']) {
                $activeRules[] = $this->loadRule($rule);
            }
        }

        return $activeRules;
    }

    /**
     * Load single rule.
     * Also loading conditions and actions associated.
     *
     * @param array $ruleData
     *
     * @return \CodingTest\Rule
     */
    public function loadRule(array $ruleData)
    {
        $action = $this->constructActionFromConfig($ruleData['action']);
        $rule = new \CodingTest\Rule($action);
        $rule->setTitle($ruleData['title']);
        foreach ($ruleData['conditions'] as $condition) {
            $rule->addCondition(
                $this->constructConditionFromConfig($condition)
            );
        }

        return $rule;
    }

    /**
     * Build action from rule configuration.
     *
     * @param array $actionConfig
     *
     * @return \CodingTest\Rule\Action
     */
    protected function constructActionFromConfig($actionConfig)
    {
        $className = "\\CodingTest\\Rule\\Action\\".$actionConfig['action'];
        if (!class_exists($className)) {
            throw new \Exception("Invalid action.");
        }
        $action = new $className;
        $this->fillParameters($action, $actionConfig['parameters']);

        return $action;
    }

    /**
     * Build condition from rule configuration.
     *
     * @param array $conditionConfig
     *
     * @return \CodingTest\Rule\Condition
     */
    protected function constructConditionFromConfig($conditionConfig)
    {
        $className = "\\CodingTest\\Rule\\Condition\\".$conditionConfig['condition'];
        if (!class_exists($className)) {
            throw new \Exception("Invalid condition.");
        }

        $condition = new $className;
        $this->fillParameters($condition, $conditionConfig['parameters']);

        return $condition;
    }

    /**
     * Fill instance parameters via setter method.
     *
     * @param object $instance
     * @param array $parameters
     *
     * @return void
     */
    protected function fillParameters($instance, $parameters)
    {
        array_walk($parameters, function($value, $key) use ($instance) {
            $methodName = $this->buildSetterMethodName($key);

            if (method_exists($instance, $methodName)) {
                $instance->$methodName($value);
            }
        });
    }

    /**
     * Put together the setter method name.
     *
     * @param string $methodName
     *
     * @return string
     */
    protected function buildSetterMethodName($methodName)
    {
        return 'set'.implode(
            '',
            array_map(
                function($word) {
                    return ucfirst($word);
                },
                explode('_',$methodName)
            )
        );
    }
}
