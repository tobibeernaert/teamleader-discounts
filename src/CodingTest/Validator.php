<?php
namespace CodingTest;

use \Respect\Validation\Validator as v;

/**
 * Validator Factory
 */
class Validator
{
    /**
     * Build order validators.
     *
     * will accept anything in the following format:
     *
     * {
     *   "id": "1",
     *   "customer-id": "1",
     *   "items": [
     *     {
     *       "product-id": "1",
     *       "quantity": "1",
     *       "unit-price": "0.00",
     *       "total": "0.00"
     *     },
     *     ...
     *   ],
     *   "total": "0.00"
     * }
     *
     * @return array
     */
    public static function buildOrderValidators()
    {
        $idValidator = v::positive();
        $productIdValidator = v::alnum();
        $qtyValidator = v::positive();
        $priceValidator = v::floatVal()->positive();

        return [
            "id" => $idValidator,
            "customer-id" => $idValidator,
            "items" => v::each(
                v::arrayVal()
                    ->key('product-id', $productIdValidator)
                    ->key('quantity', $qtyValidator)
                    ->key('unit-price', $priceValidator)
                    ->key('total', $priceValidator)
            ),
            "total" => $priceValidator,
        ];
    }
}
