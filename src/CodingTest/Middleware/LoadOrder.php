<?php
namespace CodingTest\Middleware;

/**
 * LoadOrder
 */
class LoadOrder
{
    /**
     * @var \CodingTest\Mapper\Order $orderService
     */
    protected $orderService;

    /**
     * @param \CodingTest\Mapper\Order $orderService
     */
    public function __construct(
        \CodingTest\Mapper\Order $orderService
    ) {
        $this->orderService = $orderService;
    }

    /**
     * Load order from body.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param callable $next
     *
     * @return \Psr\Http\Message\ResponseInterface $response
     */
    public function __invoke(
        \Psr\Http\Message\ServerRequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response,
        $next
    ) {

        $order = $this->orderService->loadOrder($request->getParsedBody());
        $request = $request->withAttribute('order', $order);

        return $next($request, $response);
    }
}
