<?php
namespace CodingTest\Middleware;

/**
 * ValidationErrorHandling
 */
class ValidationErrorHandling
{
    /**
     * Kill request when validation errors where found.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param callable $next
     *
     * @return \Psr\Http\Message\ResponseInterface $response
     */
    public function __invoke(
        \Psr\Http\Message\ServerRequestInterface $request,
        \Psr\Http\Message\ResponseInterface $response,
        $next
    ) {
        $errors = $request->getAttribute('errors');
        if (!empty($errors)) {
            return $response->withStatus(500)
                ->withJson([
                    "Validation error" => $errors,
                ]);
        }

        return $next($request, $response);
    }
}
