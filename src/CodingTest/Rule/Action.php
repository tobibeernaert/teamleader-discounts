<?php
namespace CodingTest\Rule;

/**
 * Action interface.
 */
interface Action
{
    /**
     * Calcualte discount.
     *
     * @param \CodingTest\Entity\Order $order
     *
     * @return float
     */
    public function calculate(\CodingTest\Entity\Order $order);
}
