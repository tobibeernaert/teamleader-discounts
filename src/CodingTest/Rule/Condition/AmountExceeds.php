<?php
namespace CodingTest\Rule\Condition;

/**
 * AmountExceeds.
 *
 * Condition that checks if a certain amounts exceeds a maximum amount.
 *
 * @see \CodingTest\Rule\Condition
 */
class AmountExceeds implements \CodingTest\Rule\Condition
{
    /**
     * @var float
     */
    protected $amount;

    /**
     * @var float
     */
    protected $maximum;

    /**
     * @param float $amount
     * @param float $maximum
     */
    public function __construct($amount = 0.00, $maximum = 0.00)
    {
        $this->amount = (float)$amount;
        $this->maximum = (float)$maximum;
    }

    /**
     * {@inheritDoc}
     */
    public function validate(\CodingTest\Entity\Order $order)
    {
        return $this->amount >= $this->maximum;
    }

    /**
     * Amount setter.
     *
     * @param float $amount
     * @return void
     */
    public function setAmount($amount)
    {
        $this->amount = (float)$amount;
    }

    /**
     * Amount getter.
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Maximum setter.
     *
     * @param float $maximum
     * @return void
     */
    public function setMaximum($maximum)
    {
        $this->maximum = (float)$maximum;
    }

    /**
     * Maximum getter.
     *
     * @return float
     */
    public function getMaximum()
    {
        return $this->maximum;
    }
}
