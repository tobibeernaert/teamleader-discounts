<?php
namespace CodingTest\Rule\Condition;

/**
 * RevenueAmountExceeds.
 *
 * Condition that checks if the customer revenu exceeds a certain amount.
 *
 * @see \CodingTest\Rule\Condition
 */
class RevenueAmountExceeds implements \CodingTest\Rule\Condition
{
    /**
     * @var float
     */
    protected $maximum;

    /**
     * @param float $maximum
     */
    public function __construct($maximum = 0.00)
    {
        $this->setMaximum($maximum);
    }

    /**
     * {@inheritDoc}
     */
    public function validate(\CodingTest\Entity\Order $order)
    {
        return $order->getCustomer()->getRevenue() >= $this->maximum;
    }

    /**
     * Maximum setter.
     *
     * @param float $maximum
     */
    public function setMaximum($maximum)
    {
        $this->maximum = (float)$maximum;
    }

    /**
     * Maximum getter.
     *
     * @return float
     */
    public function getMaximum()
    {
        return $this->maximum;
    }
}
