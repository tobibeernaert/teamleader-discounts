<?php
namespace CodingTest\Rule\Condition;

/**
 * HasProductsFromCategoryExtended.
 *
 * Condition that checks if an order had x products from a given category.
 *
 * @see \CodingTest\Rule\Condition
 */
class HasProductsFromCategoryExtended implements \CodingTest\Rule\Condition
{
    /**
     * @var int
     */
    protected $category;

    /**
     * @var int
     */
    protected $minimum;

    /**
     * @param int $category
     * @param int $minimum
     */
    public function __construct($category = 0, $minimum = 0)
    {
        $this->setCategory($category);
        $this->setMinimum($minimum);
    }

    /**
     * {@inheritDoc}
     */
    public function validate(\CodingTest\Entity\Order $order)
    {
        $quantity = 0;
        foreach ($order->getItems() as $orderItem) {
            if (!$orderItem->product->inCategory($this->category)) {
                continue;
            }

            $quantity += $orderItem->quantity;
        }

        return $quantity >= $this->minimum;
    }

    /**
     * Category getter.
     *
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    /**
     * Category setter.
     *
     * @param int $category
     * @return void
     */
    public function setCategory($category)
    {
        $this->category = (int)$category;
    }

    /**
     * Minimum getter.
     *
     * @return int
     */
    public function getMinimum()
    {
        return $this->minimum;
    }
    
    /**
     * Minimum setter.
     *
     * @param int $minimum
     * @return void
     */
    public function setMinimum($minimum)
    {
        $this->minimum = (int)$minimum;
    }
}
