<?php
namespace CodingTest\Rule\Condition;

/**
 * HasProductsFromCategory.
 *
 * Condition that checks if an order had x products from a given category.
 *
 * @see \CodingTest\Rule\Condition
 */
class HasProductsFromCategory implements \CodingTest\Rule\Condition
{
    /**
     * @var int
     */
    protected $category;

    /**
     * @var int
     */
    protected $minimum;

    /**
     * @param int $category
     * @param int $minimum
     */
    public function __construct($category = 0, $minimum = 0)
    {
        $this->setCategory($category);
        $this->setMinimum($minimum);
    }

    /**
     * {@inheritDoc}
     */
    public function validate(\CodingTest\Entity\Order $order)
    {
        if (!$order->hasItems()) return false;

        foreach ($order->getItems() as $orderItem) {
            if ($orderItem->product->inCategory($this->category)
                && (int)$this->minimum <= (int)$orderItem->quantity) {

                return true;
            }
        }

        return false;
    }

    /**
     * Category getter.
     *
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    /**
     * Category setter.
     *
     * @param int $category
     * @return void
     */
    public function setCategory($category)
    {
        $this->category = (int)$category;
    }

    /**
     * Minimum getter.
     *
     * @return int
     */
    public function getMinimum()
    {
        return $this->minimum;
    }
    
    /**
     * Minimum setter.
     *
     * @param int $minimum
     * @return void
     */
    public function setMinimum($minimum)
    {
        $this->minimum = (int)$minimum;
    }
}
