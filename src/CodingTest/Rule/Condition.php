<?php
namespace CodingTest\Rule;

/**
 * Base condition interface.
 */
interface Condition
{
    /**
     * Validate conditions.
     *
     * @param \CodingTest\Entity\Order $order
     *
     * @param boolean
     */
    public function validate(\CodingTest\Entity\Order $order);
}
