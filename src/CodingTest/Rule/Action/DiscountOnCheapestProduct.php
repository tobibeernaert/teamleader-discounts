<?php
namespace CodingTest\Rule\Action;

/**
 * DiscountOnCheapestProduct
 *
 * Calculate disounct percentage on cheapest product in category.
 *
 * @see \CodingTest\Rule\Action
 */
class DiscountOnCheapestProduct implements \CodingTest\Rule\Action
{
    /**
     * @var int
     */
    protected $category;

    /**
     * @var int
     */
    protected $percentage;

    /**
     * @param int $category
     * @param float $percentage
     */
    public function __construct($category = 0, $percentage = 0.00)
    {
        $this->setCategory($category);
        $this->setPercentage($percentage);
    }

    /**
     * {@inheritDoc}
     */
    public function calculate(\CodingTest\Entity\Order $order)
    {
        $minimum = 0.00;

        if (!$order->hasItems()) return $minimum;

        foreach ($order->getItems() as $orderItem) {
            if (!$orderItem->product->inCategory($this->category)) {
                continue;
            }

            if (0.00 === $minimum || $minimum > $orderItem->total) {
                $minimum = $orderItem->total;
            }
        }

        return $minimum * ($this->percentage/100);
    }

    /**
     * Category getter.
     *
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    /**
     * Category setter.
     *
     * @param int $category
     * @return void
     */
    public function setCategory($category)
    {
        $this->category = (int)$category;
    }
    
    /**
     * Percentage getter.
     *
     * @return float
     */
    public function getPercentage()
    {
        return $this->percentage;
    }
    
    /**
     * Percentage setter.
     *
     * @param float $percentage
     * @return void
     */
    public function setPercentage($percentage)
    {
        $this->percentage = (float)$percentage;
    }
}
