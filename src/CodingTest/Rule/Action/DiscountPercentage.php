<?php
namespace CodingTest\Rule\Action;

/**
 * DiscountPercentage.
 *
 * Calculate discount percentage on given amount.
 *
 * @see \CodingTest\Rule\Action
 */
class DiscountPercentage implements \CodingTest\Rule\Action
{
    /**
     * @var float
     */
    protected $percentage;

    /**
     * @param float $percentage
     */
    public function __construct($percentage = 0.00)
    {
        $this->setPercentage($percentage);
    }

    /**
     * {@inheritDoc}
     */
    public function calculate(\CodingTest\Entity\Order $order)
    {
        return $order->getTotal() * ($this->percentage/100);
    }

    /**
     * Percentage getter.
     *
     * @return float
     */
    public function getPercentage()
    {
        return $this->percentage;
    }
    
    /**
     * Percentage setter.
     *
     * @param float $percentage
     * @return void
     */
    public function setPercentage($percentage)
    {
        $this->percentage = (float)$percentage;
    }
}
