<?php
namespace CodingTest\Rule\Action;

/**
 * FreeProductFromCategory.
 *
 * Calculate discount amount for free products in specific category.
 *
 * @see \CodingTest\Rule\Action
 */
class FreeProductFromCategory implements \CodingTest\Rule\Action
{
    /**
     * @var int
     */
    protected $category;

    /**
     * @param int $category
     */
    public function __construct($category = 0)
    {
        $this->setCategory($category);
    }

    /**
     * {@inheritDoc}
     */
    public function calculate(\CodingTest\Entity\Order $order)
    {
        $discount = 0;

        if (!$order->hasItems()) return $discount;

        foreach ($order->getItems() as $orderItem) {
            if (!$orderItem->product->inCategory($this->category)) {
                continue;
            }

            $discount += $orderItem->unitPrice;
        }

        return $discount;
    }

    /**
     * Category getter.
     *
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    /**
     * Category setter.
     *
     * @param int $category
     * @return void
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }
}
