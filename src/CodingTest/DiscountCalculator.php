<?php
namespace CodingTest;

/**
 * DiscountCalculator
 */
class DiscountCalculator
{
    /**
     * @var \ArrayObject
     */
    protected $rules;

    /**
     * @var float
     */
    protected $discount;

    /**
     * @var array
     */
    protected $appliedRules;

    public function __construct()
    {
        $this->rules = new \ArrayObject;
        $this->reset();
    }

    /**
     * Reset parameters.
     *
     * @return void
     */
    public function reset()
    {
        $this->discount = 0.00;
        $this->appliedRules = [];
    }

    /**
     * Add rule to the pool.
     *
     * @param \CodingTest\Rule $rule
     * @return void
     */
    public function addRule(\CodingTest\Rule $rule)
    {
        $this->rules[$rule->getTitle()] = $rule;
    }

    /**
     *
     * Apply rule pool to order instance.
     *
     * @param \CodingTest\Entity\Order $order
     *
     * @return void
     */
    public function applyRules($order)
    {
        $this->reset();
        foreach ($this->rules as $ruleTitle => $rule) {
            if (0 < ($discount = $rule->process($order))) {
                $this->appliedRules[$ruleTitle] = $discount;
            }
            $this->discount += $discount;
        }
    }

    /**
     * AppliedRules getter.
     *
     * @return []
     */
    public function getAppliedRules()
    {
        return $this->appliedRules;
    }

    /**
     * Discount getter.
     *
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }
}
