<?php

namespace CodingTest;

/**
 * Rule
 */
class Rule
{
    /**
     * @var array
     */
    protected $conditions;

    /**
     * @var \CodingTest\Rule\Action
     */
    protected $action;

    /**
     * @var string
     */
    protected $title;

    /**
     * @param \CodingTest\Rule\Action $action
     */
    public function __construct (
        \CodingTest\Rule\Action $action
    ) {
        $this->conditions = [];
        $this->action = $action;
        $this->setTitle('');
    }

    /**
     * Add condition to rule.
     *
     * @param \CodingTest\Rule\Condition $condition
     */
    public function addCondition(\CodingTest\Rule\Condition $condition)
    {
        $this->conditions[] = $condition;
    }

    /**
     * Action getter.
     *
     * @return \CodingTest\Rule\Action
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Process order through rule.
     *
     * @param \CodingTest\Entity\Order $order
     *
     * @return float
     */
    public function process($order)
    {
        foreach ($this->conditions as $condition) {
            if (!$condition->validate($order)) {
                return 0.00;
            }
        }

        return $this->action->calculate($order);
    }

    /**
     * Title getter.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Title setter.
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}
