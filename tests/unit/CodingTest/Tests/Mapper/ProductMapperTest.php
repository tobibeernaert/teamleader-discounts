<?php
namespace CodingTest\Tests\Mapper;

/**
 * ProductMapperTest
 *
 * @see \PHPUnit_Framework_TestCase
 */
class ProductMapperTest extends \PHPUnit_Framework_TestCase
{
    protected $product;

    public function setUp()
    {
        $data = json_decode(file_get_contents(__DIR__.'/../../../../data/products.json'), true);
        $this->product = new \CodingTest\Mapper\Product($data);
    }

    public function providerInCategory()
    {
        return [
            ["A101", 1, true,],
            ["B103", 10, false],
        ];
    }

    /**
     * @dataProvider providerInCategory
     * @group catalog
     */
    public function testInCategory($productId, $categoryId, $expectation)
    {
        $result = $this->product->inCategory($productId, $categoryId);

        $this->assertInternalType('boolean', $result);
        $this->assertEquals($expectation, $result);
    }

    /**
     * @group catalog
     */
    public function testFindProductByIdFound()
    {
        $result = $this->product->findProductById("B101");
        $this->assertEquals("B101", $result->getId());
    }

    public function providerProductNotFound()
    {
        return [
            [100],
            [0],
            [null],
            [''],
        ];
    }

    /**
     * @dataProvider providerProductNotFound
     * @group catalog
     */
    public function testFindProductByIdNotFound($productId)
    {
        $result = $this->product->findProductById($productId);
        $this->assertNull($result);
    }

    /**
     * @group catalog
     */
    public function testFilterProductsByCategoryIdFound()
    {
        $result = $this->product->filterProductsByCategoryId(2);
        $this->assertCount(3, $result);
    }

    public function providerProductFilterNotFound()
    {
        return [
            [100],
            [0],
            [null],
            [''],
        ];
    }

    /**
     * @dataProvider providerProductFilterNotFound
     */
    public function testFilterProductsByCategoryIdNotFound($categoryId)
    {
        $result = $this->product->filterProductsByCategoryId($categoryId);
        $this->assertCount(0, $result);
    }
}
