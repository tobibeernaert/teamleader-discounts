<?php
namespace CodingTest\Tests\Mapper;

/**
 * CustomerMapperTest
 *
 * @see \PHPUnit_Framework_TestCase
 */
class CustomerMapperTest extends \PHPUnit_Framework_TestCase
{
    protected $customer;

    public function setUp()
    {
        $data = json_decode(file_get_contents(__DIR__.'/../../../../data/customers.json'), true);
        $this->customer = new \CodingTest\Mapper\Customer($data);
    }

    /**
     * @group customer
     */
    public function testFindCustomerByIdFound()
    {
        $result = $this->customer->findCustomerById(1);
        $this->assertEquals(1, $result->getId());
    }

    public function providerCustomerNotFound()
    {
        return [
            [100],
            [0],
            [null],
            [''],
        ];
    }

    /**
     * @dataProvider providerCustomerNotFound
     * @group customer
     */
    public function testFindCustomerByIdNotFound($customerId)
    {
        $result = $this->customer->findCustomerById($customerId);
        $this->assertNull($result);
    }
}
