<?php
namespace CodingTest\Test\Rule\Condition;

class AmountExceedsTest extends \PHPUnit_Framework_TestCase
{
    public function getOrder()
    {
        return new \CodingTest\Entity\Order(
            1,
            new \CodingTest\Entity\Customer(
                'Foo Bar'
            ),
            100.00
        );
    }

    public function goodValidationsProvider()
    {
        return [
            [1, 2, false],
            [-1, 2, false],
            [0, 0, true],
            [3, 2, true],
            [2, 2, true],
            [-1, -2, true],
            [null, null, true],
        ];
    }

    /**
     * @dataProvider goodValidationsProvider
     * @group conditions
     */
    public function testValidate($amount, $maximum, $validConditionResult)
    {
        $condition = new \CodingTest\Rule\Condition\AmountExceeds(
            $amount,
            $maximum
        );

        $result = $condition->validate($this->getOrder());

        $this->assertEquals($validConditionResult, $result);
    }
}
