<?php
namespace CodingTest\Test\Rule\Condition;

class HasProductsFromCategoryExtendedTest extends \PHPUnit_Framework_TestCase
{
    public function getOrder($items = [])
    {
        $order = new \CodingTest\Entity\Order(
            1,
            new \CodingTest\Entity\Customer('Foo Bar'),
            100
        );
        foreach ($items as $category => $quantity) {
            $product = new \CodingTest\Entity\Product(rand(1,10), 10.00);
            $product->setCategory($category);
            $order->addItem(
                $product,
                $quantity,
                10.00,
                (float)$quantity * 10.00
            );
        }

        return $order;
    }

    public function validationsProvider()
    {
        return [
            // these should all validate as true, as minimum
            // will always evaluate as 0.
            [null, null, $this->getOrder([]), true],
            [null, null, $this->getOrder([1 => 1]), true],
            [null, null, $this->getOrder([1 => 1, 2 => 100]), true],
            [null, null, $this->getOrder([2 => 1, 2 => 100]), true],

            // these should all validate as true, as minimum is always 0
            [1, 0, $this->getOrder([]), true],
            [1, 0, $this->getOrder([1 => 1]), true],
            [1, 0, $this->getOrder([1 => 1, 2 => 100]), true],
            [1, 0, $this->getOrder([2 => 1, 2 => 100]), true],

            [1, 10, $this->getOrder([]), false],
            [1, 10, $this->getOrder([1 => 1]), false],
            [1, 10, $this->getOrder([1 => 20, 2 => 100]), true],
            [1, 10, $this->getOrder([2 => 1, 2 => 100]), false],
            [2, 10, $this->getOrder([2 => 1, 2 => 100]), true],
            [2, 103, $this->getOrder([2 => 1, 2 => 100]), false],

            // Category is not found so they should all be false
            [10, 10, $this->getOrder([]), false],
            [10, 10, $this->getOrder([1 => 1]), false],
            [10, 10, $this->getOrder([1 => 1, 2 => 100]), false],
            [10, 10, $this->getOrder([2 => 1, 2 => 100]), false],
        ];
    }

    /**
     * @dataProvider validationsProvider
     * @group conditions
     */
    public function testValidate($category, $minimum, $order, $validConditionResult)
    {
        $condition = new \CodingTest\Rule\Condition\HasProductsFromCategoryExtended(
            $category,
            $minimum
        );

        $result = $condition->validate($order);

        $this->assertEquals($validConditionResult, $result);
    }
}
