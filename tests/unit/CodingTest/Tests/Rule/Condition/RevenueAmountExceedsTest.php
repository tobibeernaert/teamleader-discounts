<?php
namespace CodingTest\Test\Rule\Condition;

class RevenueAmountExceedsTest extends \PHPUnit_Framework_TestCase
{
    public function getOrder($revenue)
    {
        $customer = new \CodingTest\Entity\Customer('Foo Bar');
        $customer->setRevenue($revenue);
        return new \CodingTest\Entity\Order(
            1,
            $customer,
            100
        );
    }

    public function validationsProvider()
    {
        return [
            [1, $this->getOrder(100), true],
            [1000, $this->getOrder(0), false],
            [0, $this->getOrder(0), true],
            [1000, $this->getOrder(1000), true],
            [1000, $this->getOrder(100), false],
            [-1, $this->getOrder(100), true],
            [null, $this->getOrder(null), true],
            [null, $this->getOrder(100), true],
            ['', $this->getOrder(100), true],
            ['test', $this->getOrder(100), true],
        ];
    }

    /**
     * @dataProvider validationsProvider
     * @group conditions
     */
    public function testValidate($maximum, $order, $validConditionResult)
    {
        $condition = new \CodingTest\Rule\Condition\RevenueAmountExceeds(
            $maximum
        );

        $result = $condition->validate($order);

        $this->assertEquals($validConditionResult, $result);
    }
}
