<?php
namespace CodingTest\Test\Rule\Condition;

class HasProductsFromCategoryTest extends \PHPUnit_Framework_TestCase
{
    public function getOrder($items = [])
    {
        $order = new \CodingTest\Entity\Order(
            1,
            new \CodingTest\Entity\Customer('Foo Bar'),
            100
        );
        foreach ($items as $category => $quantity) {
            $product = new \CodingTest\Entity\Product(rand(1,10), 10.00);
            $product->setCategory($category);
            $order->addItem(
                $product,
                $quantity,
                10.00,
                (float)$quantity * 10.00
            );
        }

        return $order;
    }

    public function validationsProvider()
    {
        return [
            [null, null, $this->getOrder([]), false],
            [null, null, $this->getOrder([1 => 1]), false],
            [null, null, $this->getOrder([1 => 1, 2 => 100]), false],
            [null, null, $this->getOrder([2 => 1, 2 => 100]), false],

            [1, 0, $this->getOrder([]), false],
            [1, 0, $this->getOrder([1 => 1]), true],
            [1, 0, $this->getOrder([1 => 1, 2 => 100]), true],
            [1, 0, $this->getOrder([2 => 1, 2 => 100]), false],

            [1, 10, $this->getOrder([]), false],
            [1, 10, $this->getOrder([1 => 1]), false],
            [1, 10, $this->getOrder([1 => 20, 2 => 100]), true],
            [1, 10, $this->getOrder([2 => 1, 2 => 100]), false],

            [10, 10, $this->getOrder([]), false],
            [10, 10, $this->getOrder([1 => 1]), false],
            [10, 10, $this->getOrder([1 => 1, 2 => 100]), false],
            [10, 10, $this->getOrder([2 => 1, 2 => 100]), false],
        ];
    }

    /**
     * @dataProvider validationsProvider
     * @group conditions
     */
    public function testValidate($category, $minimum, $order, $validConditionResult)
    {
        $condition = new \CodingTest\Rule\Condition\HasProductsFromCategory(
            $category,
            $minimum
        );

        $result = $condition->validate($order);

        $this->assertEquals($validConditionResult, $result);
    }
}
