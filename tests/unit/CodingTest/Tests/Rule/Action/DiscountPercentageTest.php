<?php
namespace CodingTest\Test\Rule\Action;

class DiscountPercentageTest extends \PHPUnit_Framework_TestCase
{
    public function getOrder($total)
    {
        return new \CodingTest\Entity\Order(
            1,
            new \CodingTest\Entity\Customer(
                'Foo Bar'
            ),
            $total
        );
    }

    public function goodCalculationsProvider()
    {
        return [
            [null, null, 0],
            [null, 100, 0],
            [10, null, 0],
            [10, 0, 0],
            [10, 100, 10],
            [10, 100, 10],
            [100, 100, 100],
            [10, 123.45, 12.35],
        ];
    }

    /**
     * @dataProvider goodCalculationsProvider
     * @group actions
     */
    public function testCalculate($percentage, $total, $validActionResult)
    {
        $condition = new \CodingTest\Rule\Action\DiscountPercentage(
            $percentage
        );

        $result = number_format(
            $condition->calculate($this->getOrder($total)),
            2
        );

        $this->assertEquals($validActionResult, $result);
    }
}
