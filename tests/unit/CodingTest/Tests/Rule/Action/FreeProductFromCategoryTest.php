<?php
namespace CodingTest\Test\Rule\Action;

class FreeProductFromCategoryTest extends \PHPUnit_Framework_TestCase
{
    public function getOrder($items)
    {
        $order = new \CodingTest\Entity\Order(
            1,
            new \CodingTest\Entity\Customer(
                'Foo Bar'
            ),
            100.00
        );

        foreach ($items as $item) {
            list($category, $unitPrice) = $item;
            $product = new \CodingTest\Entity\Product(rand(1,10), 10.00);
            $product->setCategory($category);
            $order->addItem(
                $product,
                1,
                $unitPrice,
                10.00
            );
        }

        return $order;
    }

    public function goodCalculationsProvider()
    {
        return [
            [null, $this->getOrder([]), 0.00],
            [null, $this->getOrder([[1, 10]]), 0.00],
            [null, $this->getOrder([[1, 10], [2, 20]]), 0.00],
            [null, $this->getOrder([[2, 10], [2, 20]]), 0.00],

            [1, $this->getOrder([]), 0.00],
            [1, $this->getOrder([[1, 10]]), 10.00],
            [1, $this->getOrder([[1, 10], [2, 20]]), 10.00],
            [1, $this->getOrder([[2, 10], [2, 20]]), 0.00],
            [2, $this->getOrder([[2, 20], [2, 20]]), 40.00],

            // Category is not found so they should all be 0
            [10, $this->getOrder([]), 0.00],
            [10, $this->getOrder([[1, 10]]), 0.00],
            [10, $this->getOrder([[1, 10], [2, 20]]), 0.00],
            [10, $this->getOrder([[2, 20], [2, 20]]), 0.00],
        ];
    }

    /**
     * @dataProvider goodCalculationsProvider
     * @group actions
     */
    public function testCalculate($category, $order, $validActionResult)
    {
        $condition = new \CodingTest\Rule\Action\FreeProductFromCategory(
            $category
        );

        $result = number_format(
            $condition->calculate($order),
            2
        );

        $this->assertEquals($validActionResult, $result);
    }
}
