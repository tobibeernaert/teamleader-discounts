<?php
namespace CodingTest\Test\Rule\Action;

class DiscountOnCheapestProductTest extends \PHPUnit_Framework_TestCase
{
    public function getOrder($items)
    {
        $order = new \CodingTest\Entity\Order(
            1,
            new \CodingTest\Entity\Customer(
                'Foo Bar'
            ),
            100.00
        );

        foreach ($items as $item) {
            list($category, $itemTotal) = $item;
            $product = new \CodingTest\Entity\Product(rand(1,10), 10.00);
            $product->setCategory($category);
            $order->addItem(
                $product,
                1,
                10.00,
                (float)$itemTotal
            );
        }

        return $order;
    }

    public function goodCalculationsProvider()
    {
        return [
            // percentage null evaluates to 0, which will also be a 0 result
            [null, null, $this->getOrder([]), 0.00],
            [null, null, $this->getOrder([[1, 1]]), 0.00],
            [null, null, $this->getOrder([[1, 1], [2, 100]]), 0.00],
            [null, null, $this->getOrder([[2, 1], [2, 100]]), 0.00],

            // 0 percentage will always return 0 result
            [1, 0, $this->getOrder([]), 0.00],
            [1, 0, $this->getOrder([[1, 1]]), 0.00],
            [1, 0, $this->getOrder([[1, 1], [2,  100]]), 0.00],
            [1, 0, $this->getOrder([[2, 1], [2, 100]]), 0.00],

            [1, 10, $this->getOrder([]), 0.00],
            [1, 10, $this->getOrder([[1, 1]]), 0.10],
            [1, 10, $this->getOrder([[1, 20], [2, 100]]), 2.00],
            [1, 10, $this->getOrder([[2, 20], [2, 100]]), 0.00],
            [2, 10, $this->getOrder([[2, 20], [2, 100]]), 2.00],

            // Category is not found so they should all be false
            [10, 10, $this->getOrder([]), 0.00],
            [10, 10, $this->getOrder([[1, 1]]), 0.00],
            [10, 10, $this->getOrder([[1, 20], [2, 100]]), 0.00],
            [10, 10, $this->getOrder([[2, 20], [2, 100]]), 0.00],
        ];
    }

    /**
     * @dataProvider goodCalculationsProvider
     * @group actions
     */
    public function testCalculate($category, $percentage, $order, $validActionResult)
    {
        $condition = new \CodingTest\Rule\Action\DiscountOnCheapestProduct(
            $category,
            $percentage
        );

        $result = number_format(
            $condition->calculate($order),
            2
        );

        $this->assertEquals($validActionResult, $result);
    }
}
